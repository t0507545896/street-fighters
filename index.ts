import App from './src/javascript/app';
import './src/styles/styles.css';


class Greeter {
    greeting: string;
    constructor(message: string) {
        this.greeting = message;
    }
    greet() {
        return "Hello, " + this.greeting;
    }
}

let greeter = new Greeter("hey hey hey");


console.log(greeter.greet());

new App();