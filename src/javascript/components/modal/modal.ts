import { createElement } from '../../helpers/domHelper';

type ModalInfo = { 
  title: string, 
  bodyElement: HTMLElement, 
  onClose: () => void
};

export function showModal(info: ModalInfo): void {
  const root = getModalContainer();
  const modal = createModal(info); 
  
  root.append(modal);
}

function getModalContainer() {
  return document.getElementById('root');
}

function createModal(info: ModalInfo): HTMLElement {
  const layer = createElement({ tagName: 'div', className: 'modal-layer' });
  const modalContainer = createElement({ tagName: 'div', className: 'modal-root' });
  const header = createHeader(info.title, info.onClose);

  modalContainer.append(header, info.bodyElement);
  layer.append(modalContainer);

  return layer;
}

function createHeader(title: string, onClose: () => void): HTMLElement {
  const headerElement = createElement({ tagName: 'div', className: 'modal-header' });
  const titleElement = createElement({ tagName: 'span' });
  const closeButton = createElement({ tagName: 'div', className: 'close-btn' });
  
  titleElement.innerText = title;
  closeButton.innerText = '×';
  
  const close = () => {
    hideModal();
    onClose();
  }
  closeButton.addEventListener('click', close);
  headerElement.append(titleElement, closeButton);
  
  return headerElement;
}

function hideModal(): void {
  const modal = document.getElementsByClassName('modal-layer')[0];
  modal && modal.remove();
}
