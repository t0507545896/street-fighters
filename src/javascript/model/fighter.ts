interface Fighter {
    _id: string;
    name: string;
    source: string;
    attack?: number;
    defense?: number;
    health?: number;
    remainingHealth?: number;
}

export default Fighter;