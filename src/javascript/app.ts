import { createFighters } from './components/fightersView';
import { fighterService } from './services/fightersService';

class App {

  constructor() {
    this.startApp();
  }

  
  async startApp() {
    let rootElement = document.getElementById('root');
    let loadingElement = document.getElementById('loading-overlay');

    try {
      loadingElement.style.visibility = 'visible';
      const fighters = await fighterService.getFighters();      
      const fightersElement = createFighters(fighters);

      rootElement.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      rootElement.innerText = 'Failed to load data';
    } finally {
      loadingElement.style.visibility = 'hidden';
    }
  }
}

export default App;
