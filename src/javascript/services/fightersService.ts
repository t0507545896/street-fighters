import { callApi } from '../helpers/apiHelper';
import Fighter from '../model/fighter';


class FighterService {

  public async getFighters(): Promise<Fighter[]> {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      const fighters: Fighter[] = [];
      for (let rawFighter of apiResult) {
        const fighter: Fighter = this.createFighterFromApiResult(rawFighter);  
        fighters.push(fighter);
      }
      return fighters;
    } catch (error) {
      throw error;
    }
  }

  public async getFighterDetails(id: string): Promise<Fighter> {
    // todo: implement this method
    // endpoint - `details/fighter/${id}.json`;
    const endpoint = `details/fighter/${id}.json`;
    const apiResult = await callApi(endpoint, 'GET');

    const fighter: Fighter = this.createFighterFromApiResult(apiResult);
    return fighter;
  }

  private createFighterFromApiResult(rawFighter: any): Fighter {
    const result: Fighter = {
      _id: rawFighter._id,
      name: rawFighter.name,
      attack: rawFighter.attack,
      defense: rawFighter.defense,
      health: rawFighter.health,
      source: rawFighter.source
    };
    return result;
  }

}

export const fighterService = new FighterService();
