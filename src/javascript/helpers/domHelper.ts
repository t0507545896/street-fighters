type ElementData = { 
  tagName: string, 
  className?: string, 
  attributes?: {
    [key: string]: any
  }
};

export function createElement(data: ElementData): HTMLElement {
  const element = document.createElement(data.tagName);

  if (data.className) {
    const classNames = data.className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  const attrs = data.attributes ?? {};

  Object.keys(attrs).forEach((key) => element.setAttribute(key, attrs[key]));

  return element;
}
